package com.learning.swagger.controller;

import com.learning.swagger.domain.dto.BookDTO;
import com.learning.swagger.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/books")
public class BookRestController {
    BookService bookService;

    @Autowired
    public BookRestController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/{isbn}")
    public ResponseEntity<BookDTO> findByISBN(@PathVariable("isbn") String isbn) {
        BookDTO bookDTO = bookService.findByISBN(isbn);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(bookDTO);
    }
}
