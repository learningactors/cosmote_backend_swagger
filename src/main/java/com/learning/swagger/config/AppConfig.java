package com.learning.swagger.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean("modelMapper")
    ModelMapper getMapper() {
        return new ModelMapper();
    }
}
