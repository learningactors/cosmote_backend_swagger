package com.learning.swagger.repository;

import com.learning.swagger.domain.entity.BookEntity;

import java.util.Optional;

public interface BookRepository {
    Optional<BookEntity> findByISBN(String isbn);
}
