package com.learning.swagger.repository;

import com.learning.swagger.domain.entity.BookEntity;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class BookRepositoryImpl implements BookRepository {
    private static final List<BookEntity> BOOKS = new ArrayList<>();
    static {
        BOOKS.add(new BookEntity("ISBN-1", "War and Peace", "1867", new BigDecimal(10)));
    }

    @Override
    public Optional<BookEntity> findByISBN(String isbn) {
        return BOOKS.stream()
                .filter(b -> b.getIsbn().equalsIgnoreCase(isbn))
                .findAny();
    }
}
