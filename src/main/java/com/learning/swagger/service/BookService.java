package com.learning.swagger.service;

import com.learning.swagger.domain.dto.BookDTO;

public interface BookService {
    BookDTO findByISBN(String isbn);
}
