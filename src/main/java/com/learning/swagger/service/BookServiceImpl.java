package com.learning.swagger.service;

import com.learning.swagger.domain.dto.BookDTO;
import com.learning.swagger.domain.entity.BookEntity;
import com.learning.swagger.repository.BookRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {
    BookRepository bookRepository;
    ModelMapper modelMapper;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository, ModelMapper modelMapper) {
        this.bookRepository = bookRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public BookDTO findByISBN(String isbn) {
        Optional<BookEntity> bookEntity = bookRepository.findByISBN(isbn);
        return bookEntity.map(b -> modelMapper.map(b, BookDTO.class)).orElse(null);
    }
}
